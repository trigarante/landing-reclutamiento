import createPersistedState from 'vuex-persistedstate'

export default ({store}) => {
  createPersistedState({
    key: 'sazke',
    storage:window.sessionStorage

  })(store)
}
